from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///shop.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    surname = db.Column(db.String(300), nullable=False)
    number = db.Column(db.Text)

    def __repr__(self):
        return '<Client %r>' % self.id


@app.route('/')
def hello_world():  # put application's code here
    return render_template('hello.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/create-page', methods=['POST', 'GET'])
def create_page():
    if request.method == "POST":
        surname = request.form['surname']
        name = request.form['name']
        number = request.form['number']

        client = Client(surname=surname, name=name, number=number)

        try:
            db.session.add(client)
            db.session.commit()
            return redirect('/')
        except:
            return print("При добавлении товара произошла ошибка: ")
    else:
        return render_template('create_page.html')


    if __name__ == '__main__':
        app.run(debug=True)